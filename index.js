const request = require('request')
const express = require('express');
const bodyParser = require('body-parser');
const isProd = () => {
    return !!process.env.AWS_REGION
}
const app = express();
app.use(bodyParser.urlencoded({
    extended: true
}));
if (isProd()) {
    console.log('Environment: Production');
    const awsServerlessExpressMiddleware = require("aws-serverless-express/middleware");
    app.use(awsServerlessExpressMiddleware.eventContext());
} 
// slack が認可コードを送ってくる先の URL を設定
// 実際には、一旦デプロイした後、 
// API Gateway のエンドポイント(https://xxxxxx.us-east-2.amazonaws.com/prod/)
// を確認してから記載
const slack_redirect_url = 'https://wb8mtufu8k.execute-api.us-east-2.amazonaws.com/prod/slackendpoint'
// Slackの認可コード受け取りのためのエンドポイント
app.get('/slackendpoint', (req, res) => {
    // 認可コードの取得
    const code = req.query["code"];
    const url = `https://pomodoro-tec-timer.auth.us-east-2.amazoncognito.com/oauth2/idpresponse?state=${req.query.state}&code=${code}&scope=openid`
    res.redirect(url);
});
// Cognito からの認証要求を受け付けるエンドポイント
app.get('/authorization', (req, res) => {
    console.log("auth", req)
    const redirect_uri = req.query.redirect_uri
    const state = req.query.state;
    const client_id = req.query.client_id;
    const url = `https://slack.com/oauth/authorize?client_id=${client_id}&scope=identify&redirect_uri=${slack_redirect_url}&state=${state}&target_uri=${redirect_uri}`;
   res.redirect(url);
);
// Cognito からのアクセストークンの要求を受け付けるエンドポイント
app.post('/token', (req, res, next) => {
   console.log("token", req);
   const client_id = req.body.client_id;
   const client_secret = req.body.client_secret;
   request({
       url: "https://slack.com/api/oauth.access",
       method: "POST",
       form: {
           client_id: client_id,
           client_secret: client_secret,
           code: req.body.code,
           redirect_uri: slack_redirect_url
       }
   }, (error, response, body) => {
       res.setHeader('Content-Type', 'application/json');
       const param = JSON.parse(body);
       const access_token = param['access_token'] // アクセストークン
       const j = {
           access_token: access_token,
           expires_in: 3600,
           "token_type": "Bearer",
       }
       res.send(j)
   })
)
 
// Cognito からのユーザー情報の要求を受け付けるエンドポイント
app.get('/userinfo', (req, res) => {
   res.setHeader('Content-Type', 'application/json');
   const access_token = req.headers.authorization.split(' ')[1];
   console.log("userinfo", req)
   // ユーザIDを取得するためのリクエスト
  request("https://slack.com/api/auth.test", {
      method: "POST",
      form: {
          token: access_token
      }
  }, (error, response, body) => {
      const user = JSON.parse(body);
      console.log(user);
      // アクセストークンを使ってユーザ情報をリクエスト
      request("https://slack.com/api/users.info ", {
          method: 'POST',
          form: {
              token: access_token,
              user: user.user_id
          }
      }, (error, response, body) => {
          const params = JSON.parse(body).user;
          params.sub = params.id
          params.email = params.profile.email
          console.log(params)
          res.send(params)
      })
  })
})
if (isProd()) {
    module.exports = app;
} else {
    app.listen(4000, () => console.log(`Listening on: 4000`));
}